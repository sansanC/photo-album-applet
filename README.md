﻿# 微信小程序
## 微信小程序电商源码：相册资源存储小程序。

#### 项目介绍

	此项目是一套可用于存储一些图片、图片组、视频等的文件，并可被下载使用的系统

#### 功能介绍

	1. 可在后台存储海报
	2. 可在后台存储组图
	3. 可在后台存储视频
	4. 可在后台发布圈子
	5. 可在小程序端下载相应资源
	6. 其他基本功能

    - Tip 更换页面，在app.json里面设置路径即可
	
### QQ交流群 — 24934459
### 公司官网 - http://www.fz33.net  官网
### 公司其他项目案例

***共享充电宝：https://gitee.com/sansanC/sharing-power-bank-app***

多门店派单：https://gitee.com/sansanC/multiple-stores-send-single-applet 

在线课程：https://gitee.com/sansanC/online-course-applet 

健身馆：https://gitee.com/sansanC/gym-app 

派单：https://gitee.com/sansanC/dispatch-applet 

场馆预定：https://gitee.com/sansanC/venue-booking-procedures 

社区团购小程序：https://gitee.com/sansanC/community-group-buying-app 

早餐线上预订：https://gitee.com/sansanC/breakfast-subscription-applet 

相册资源存储https://gitee.com/sansanC/photo-album-applet 

美容美发：https://gitee.com/sansanC/beauty-salon-small-program

商城小程序：https://gitee.com/sansanC/wechatApp

按摩小程序：https://gitee.com/sansanC/massage-applet

### 管理后台效果图（部分图）

|登录入口：http://www.sansancloud.com/manager/#/login|试用账号：yanshi 密码：yanshi123
|:----|:----:|
|![file-list](https://image1.sansancloud.com/xianhua/2021_3/24/14/21/16_446.jpg)|![file-list](https://image1.sansancloud.com/xianhua/2021_3/24/14/21/16_385.jpg)
|![file-list](http://image1.sansancloud.com/xianhua/2021_3/24/14/21/15_952.jpg)|![file-list](http://image1.sansancloud.com/xianhua/2021_3/24/14/21/16_66.jpg)
### 效果图---扫码查看

|扫码预览|扫码预览|扫码预览
|:----|:----:|:----:|
|![file-list](http://image1.sansancloud.com/xianhua/2021_3/24/14/35/37_408.jpg)|![file-list](http://image1.sansancloud.com/xianhua/2021_3/24/14/35/37_154.jpg)|![file-list](http://image1.sansancloud.com/xianhua/2021_3/24/14/35/37_69.jpg)
|![file-list](http://image1.sansancloud.com/xianhua/2021_3/24/14/35/37_56.jpg)|![file-list](http://image1.sansancloud.com/xianhua/2021_3/24/14/35/37_212.jpg)|![file-list](http://image1.sansancloud.com/xianhua/2021_3/24/14/35/37_249.jpg)


### 公司资质

|省高薪证书|国高薪证书|
|:----|:----:|
|![file-list](http://image1.sansancloud.com/xianhua/2021_3/24/15/0/8_826.jpg)|![file-list](http://image1.sansancloud.com/xianhua/2021_3/24/15/0/6_933.jpg)
    

|![file-list](http://image1.sansancloud.com/xianhua/2021_3/24/14/43/38_791.jpg)