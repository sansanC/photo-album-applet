const app = getApp()
Component({
  properties: {
    receiveData: {
      type: JSON,
    },
  },
  data: {
      items:null,
      width:'',
      height:'',
      imagePadding:''
  },
  ready:function(){
    console.log('=====ready-MoFang====', this.data.receiveData)
    this.setData({ 
        items: this.data.receiveData.jsonData.items || [],
        width: Number(this.data.receiveData.jsonData.width)||0,
        height: Number(this.data.receiveData.jsonData.height) || 0,
        imagePadding: Number(this.data.receiveData.jsonData.imagePadding) || 0
    })
    console.log('=====MoFang-width-height-imagePadding====', this.data.width,this.data.height,this.data.imagePadding)
  },
  methods: {
    tolinkUrl: function (e) {
      let linkUrl = e.currentTarget.dataset.link
      app.globalData.linkEvent(linkUrl)
    }
  }
})