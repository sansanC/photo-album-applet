const app = getApp();
Component({
  properties: {
    receiveData: {
      type: JSON,
      value: 'default value',
    }
  },
  data: {
    someData: {},
    listData: {
      cells: [
        {
          iconPath: "https://image1.sansancloud.com/xianhua/2019_3/27/15/37/19_117.jpg?x-oss-process=style/preview_120",
          linkUrl: "address.html",
          text: "收货地址",
          color: "#777777"
        },
        {
          iconPath: "https://image1.sansancloud.com/xianhua/2019_3/27/15/37/19_432.jpg?x-oss-process=style/preview_120",
          linkUrl: "my_favorite.html",
          text: "我的收藏",
          color: "#777777"
        },
        {
          iconPath: "https://image1.sansancloud.com/xianhua/2019_3/27/15/37/19_434.jpg?x-oss-process=style/preview_120",
          linkUrl: "my_coupons.html",
          text: "优惠券",
          color: "#777777"
        },
        {
          iconPath: "https://image1.sansancloud.com/xianhua/2019_3/27/15/37/19_429.jpg?x-oss-process=style/preview_120",
          linkUrl: "",
          id: "1",
          text: "在线客服",
          color: "#777777"
        },
        {
          iconPath: "https://image1.sansancloud.com/xianhua/2019_3/27/15/37/19_420.jpg?x-oss-process=style/preview_120",
          linkUrl: "custom_page_my_newlist?pageNage=常见问题",
          text: "常见问题",
          color: "#777777"
        }
      ],
      column: 3,
      showType: 0,
      scrollNum:0,
      rowNumList:1,
    },
  },
  ready: function () {
    let that = this;
    if(that.data.receiveData.androidTemplate == "slide_navigation"){
        let rowNumList=1;
        let cellLength=that.data.receiveData.jsonData.cells.length;
        let column=that.data.receiveData.jsonData.column;
        if(cellLength>5){
           rowNumList = Math.ceil(cellLength/Number(column))
        }
        that.setData({rowNumList:rowNumList})
    }
    app.globalData.consoleFun("=====gridist组件-导航=====",[that.data.receiveData])
  },
  methods: {
    scrollNumFun:function(e){
        console.log("===scrollNumFun===",e)
        let that=this;
        let width = e.detail.scrollWidth//滚动区域宽度
        let left = e.detail.scrollLeft<0?0:e.detail.scrollLeft//滚动距离
        // let scrollNum = that.data.scrollNum//已滚动的距离
        let windowWidth= wx.getSystemInfoSync().windowWidth
        let scrollLeftNum=width-windowWidth;
        let lineNum=0;
        let scrollNum=(left==0?0:(left/scrollLeftNum*100))>100?100-lineNum*2:((left/scrollLeftNum*100)-lineNum*2<0?0:((left/scrollLeftNum*100)-lineNum*2));
        console.log(windowWidth,scrollLeftNum,scrollNum,left,lineNum)
        that.setData({
          scrollNum: scrollNum
        })
        
    },
    tolinkUrl: function (event) {
    app.globalData.consoleFun("=====gridist组件-tolinkUrl=====",[event.currentTarget.dataset.link])
      app.globalData.linkEvent(event.currentTarget.dataset.link);
    }
  },
})