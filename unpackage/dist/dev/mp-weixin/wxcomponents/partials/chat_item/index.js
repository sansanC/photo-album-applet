const app = getApp();
Component({
  properties: {
    receiveData: {
      type: JSON,
      value: 'default value',
    }
  },
  data: {
    someData: {},
    sysWidth:"",
  },
  ready:function(){
    let that=this;
    console.log("====chat-item====",that.data.receiveData)
    that.setData({
      loginUser: app.globalData.loginUser,
      setting:app.globalData.setting
    });
  },
  methods: {
    lookBigImage:function(){
      let that=this;
      console.log("===lookBigImage=====",)
      that.triggerEvent('resEvent',"") //myevent自定义名称事件，父组件中使用
    },
    buttom: function () {
      app.globalData.wxLogin(1011)
    },
    tolinkUrl: function (event) {
      console.log(event.currentTarget.dataset.link)
      app.globalData.linkEvent(event.currentTarget.dataset.link);
    }
  },
})