const app = getApp();
Component({
  properties: {
    receiveData: {
      type: JSON,
      value: 'default value',
    }
  },
  data: {
    someData: {}
  },
  methods: {
    tolinkUrl: function (event) {
      app.globalData.consoleFun("=====help_blank组件-tolinkUrl=====",[event.currentTarget.dataset.link])
      app.globalData.linkEvent(event.currentTarget.dataset.link);
    }
  },
})