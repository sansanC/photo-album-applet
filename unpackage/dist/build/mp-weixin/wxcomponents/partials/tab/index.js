const app = getApp();
Component({
  properties: {
    receiveData: {
      type: JSON,
      value: '',
    }
  },
  data: {
    someData: {},
    sysWidth: "",
    arr: [],
    currentTab: 0,
    currentPageName: "",
    show: true,
    scroll:0,
    firstDistance:'',
    sendData:'',
  },
  ready: function () {
    let that = this;
    console.log("====tab====", that.data.receiveData)
    that.setData({
      sysWidth: app.globalData.sysWidth,
      arr: that.data.receiveData.jsonData.tabs
    });
    console.log("===arr===",that.data.arr)
    let firstTabData = that.data.receiveData.jsonData.tabs[0];
    let linkUrl = firstTabData.linkUrl;
    let pageName = linkUrl.replace("custom_page_", "").replace(".html", "")
    let sendData = JSON.stringify({ title: 'noTitle', url: pageName ,params: {
        pageObjectId: that.data.receiveData.pageObjectId||0,
        pageObjectType: that.data.receiveData.pageObjectType||0
      }})
      console.log("==that.selectComponent('.custom_page')===",that.selectComponent('.custom_page'))
    that.selectComponent('.custom_page').initFun(sendData);
    if (pageName!='index'){
      that.setData({
        currentTab: 0,
        sendData: sendData
      })
    }
    that._observer = wx.createIntersectionObserver(that);
    console.log("===observer===",that.observer)
    that._observer.relativeTo('.tab_page_list').observe('.tab_page_item', (res) => {
        console.log("===res===",res)
        if (!that.data.firstDistance) {
          that.setData({
            firstDistance: res.boundingClientRect.top,
          })
        }
    })
  },
  methods: {
    // 点击标题切换当前页时改变样式
    renderPage: function (linkUrl, index){
      let that = this;
      // that.setData({ show: false })
      console.log("==that.selectComponent('.custom_page')===",that.selectComponent('.custom_page'))
      app.globalData.showToastLoading('loading', true)
      let pageName = linkUrl.replace("custom_page_", "").replace(".html", "")
      let sendData = JSON.stringify({ title: 'noTitle', url: pageName ,params: {
        pageObjectId: that.data.receiveData.pageObjectId||0,
        pageObjectType: that.data.receiveData.pageObjectType||0
      }})
      if (that.data.currentTab == index) {
        console.log("当前")
        // setTimeout(function(){
        //     that.setData({ show: true })
        // },100)
        that.selectComponent('.custom_page').initFun(sendData);
        wx.hideLoading();
        return false;
      } else {
        console.log("其他")
        that.setData({
          currentTab: index,
          sendData: sendData
        })
        that.selectComponent('.custom_page').initFun(sendData);
        // setTimeout(function(){
        //     that.setData({ show: true })
        // },100)
      }
      if (that.data.show) {
        wx.hideLoading();
      }
      console.log("======currentPageName=====", that.data.currentPageName)
    },
    swichNav: function (e) {
      let that = this;
      console.log("===e====", e)
      let index = e.currentTarget.dataset.index;
      // let currentItem = e.currentTarget.dataset.item;
      // let linkUrl = currentItem?currentItem.linkUrl:'';
      let linkUrl = e.currentTarget.dataset.url||'';
      that.renderPage(linkUrl, index)
    },
    scrollTo:function(object){
      let that=this;
      that.setData({
        scroll: object.scrollTop
      })
    },
    //判断当前滚动超过一屏时，设置tab标题滚动条。
    checkCor: function () {
      if (this.data.currentTab > 4) {
        this.setData({
          scrollLeft: 300
        })
      } else {
        this.setData({
          scrollLeft: 0
        })
      }
    },
  },
})