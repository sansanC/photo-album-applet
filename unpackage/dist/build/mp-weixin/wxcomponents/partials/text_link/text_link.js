const app = getApp();
Component({
  properties: {
    receiveData: {
      type: JSON,
      value: 'default value',
    }
  },
  data: {
    someData: {}
  },
  ready: function () {
      console.log("=====text_link=====",this.data.receiveData)
  },
  methods: {
    tolinkUrl: function (event) {
      app.globalData.consoleFun("=====text_link组件-tolinkUrl=====",[event.currentTarget.dataset.link])
      app.globalData.linkEvent(event.currentTarget.dataset.link);
    }
  },
})