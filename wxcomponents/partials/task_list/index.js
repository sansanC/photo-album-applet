const app = getApp();
Component({
  properties: {
    receiveData: {
      type: Object,
      value: 'default value',
    },
  },
  data: {
    setting: null, 
    taskList: null,
    limitState:0,
    params:null,
    listPage: {
      page: 1,
      pageSize: 0,
      totalSize: 0,
      curpage: 1,
      taskTypeId:0,
    },
  },
  ready: function () {
    let that = this;
    console.log("====taskList-data=====", that.data.receiveData);
    that.initSetting();
    if (that.data.receiveData.jsonData ) {
        that.data.params=Object.assign({},that.data.params,that.data.receiveData.jsonData)
        that.getData(that.data.params)
        if(that.data.receiveData.jsonData.count){
            that.setData({ limitState: that.data.receiveData.jsonData.count })
        }
    }else{
        that.data.listPage=Object.assign({},that.data.listPage,that.data.receiveData)
        that.getData(that.data.listPage)
    }
    console.log("====taskList-limitState=====", that.data.limitState)
    console.log("====taskList-params=====", that.data.params)
  },
  methods: {
    setDataFun:function(data){
        let that=this;
        console.log("setDataFun",data)
        that.setData({params: data})
        that.getData(data)
    },
    /* 获取数据 */
    getData: function (param) {
      var that = this
      let params=Object.assign({},param,{
          taskStatus:-1
      })
      that.setData({listPage:param})
      console.log("===params==",params)
      var customIndex = app.globalData.AddClientUrl("/wx_find_tasks.html", params)
      app.globalData.showToastLoading('loading', true)
      wx.request({
        url: customIndex.url,
        header: app.globalData.header,
        success: function (res) {
          wx.hideLoading()
          console.log(res.data)
          let data=res.data.relateObj.result
          that.data.listPage.pageSize = res.data.relateObj.pageSize
          that.data.listPage.page = res.data.relateObj.curPage
          that.data.listPage.totalSize = res.data.relateObj.totalSize
          let dataArr = that.data.taskList;
          let tagArray=[];
          for (let i = 0; i < data.length; i++) {
            if (data[i].tags && data[i].tags != '') {
              tagArray = JSON.parse(data[i].tags)
              data[i].tagArray = tagArray;
            }
            if (data[i].managerTags && data[i].managerTags != '') {
              tagArray = JSON.parse(data[i].managerTags)
              if(data[i].payedPrice>0){
                  tagArray.splice(tagArray.length,1,'已交定金')
              }
              data[i].managerTagArray = tagArray;
            }
          }
          console.log("===data====",data)
          if (param.page == 1) {
            dataArr = []
          }
          if (!data || data.length == 0) {
            that.setData({ taskList: [] })
          } else {
            if (dataArr == null) { dataArr = [] }
            dataArr = dataArr.concat(data)
            that.setData({ taskList: dataArr })
          }
          wx.hideLoading()
        },
        fail: function (res) {
          console.log("fail")
          wx.hideLoading()
          app.globalData.loadFail()
        }
      })
    },
    calling: function (e) {
      console.log('====e===', e)
      let phoneNumber = e.currentTarget.dataset.phonenumber
      wx.makePhoneCall({
        phoneNumber: phoneNumber, //此号码并非真实电话号码，仅用于测试
        success: function () {
          console.log("拨打电话成功！")
        },
        fail: function () {
          console.log("拨打电话失败！")
        }
      })
    },
    toIndex() {
      app.globalData.toIndex()
    },
    tolinkUrl: function (e) {
      let linkUrl = e.currentTarget.dataset.link
      app.globalData.linkEvent(linkUrl)
    },
    initSetting() {
      this.setData({ 
        setting: app.globalData.setting ,
        defaultColor:app.globalData.setting.platformSetting.defaultColor,
        secondColor:app.globalData.setting.platformSetting.secondColor,
       })
    },
    pullDownRefreshFun: function () {
      this.data.listPage.page = 1
      this.getData(this.data.listPage)

    },
    reachBottomFun: function () {
      var that = this;
      console.log("that.data.listPage",that.data.listPage)
      if (that.data.listPage.totalSize > that.data.listPage.curPage * that.data.listPage.pageSize) {
        that.data.listPage.page++
        that.getData(this.data.listPage);
      }else{
          console.log("到底了")
          wx.showToast({
            title: "到底了~",
            image: '/wxcomponents/images/icons/tip.png',
            duration: 1000
          });
      }
    },
  }
})