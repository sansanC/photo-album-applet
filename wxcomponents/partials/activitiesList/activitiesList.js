const app = getApp();
Component({
  properties: {
    receiveData: {
      type: JSON,
      value: 'default value',
    }
  },
  data: {
    someData: {},
    actEndTimeList: [],
    countDownList:[],
    dataArr:[],
    setting:{}
  },

  ready: function (options) {
    var me = this;
    var oldData = me.data;
    app.globalData.consoleFun("=====activitiesList组件-data=====",[me.data.receiveData])
    me.setData({ setting: app.globalData.setting, defaultColor: app.globalData.setting.platformSetting.defaultColor})
    // 已经开始的活动
    if (oldData.receiveData.relateBean&&oldData.receiveData.relateBean.length>0&&oldData.receiveData.relateBean[0].promotionStatus == 1){
      var arr=[];
      var dataLength = oldData.receiveData.relateBean.length;
      // 循环出项目的个数,添加到arr中
      for (var a = 0; a < dataLength; a++) {
        arr.push(oldData.receiveData.relateBean[a].endDate)   
      }
      me.setData({ actEndTimeList: arr })
      var interval = setInterval(function () {
        // 获取当前时间，同时得到活动结束时间数组
        let newTime = new Date().getTime();
        let endTimeList = oldData.actEndTimeList;
        let countDownArr = [];
        // 对结束时间进行处理渲染到页面
        endTimeList.forEach(o => {
          let endTime = new Date(Date.parse(o.replace(/-/g, "/"))).getTime();
          let obj = null;
          // 如果活动未结束，对时间进行处理
          if (endTime - newTime > 0) {
            let time = (endTime - newTime) / 1000;
            let day = parseInt(time / (60 * 60 * 24));
            let hou = parseInt(time % (60 * 60 * 24) / 3600);
            let min = parseInt(time % (60 * 60 * 24) % 3600 / 60);
            let sec = parseInt(time % (60 * 60 * 24) % 3600 % 60);
            obj = {
              day: this.timeFormat(day),
              hou: this.timeFormat(hou),
              min: this.timeFormat(min),
              sec: this.timeFormat(sec)
            }
          } else {
            obj = {
              day: '00',
              hou: '00',
              min: '00',
              sec: '00'
            }
          }
          countDownArr.push(obj);
        })
        this.setData({ countDownList: countDownArr })
      }.bind(this), 1000);
    }
    // 还未开始的活动
    if (oldData.receiveData.relateBean&&oldData.receiveData.relateBean.length>0&&oldData.receiveData.relateBean[0].promotionStatus == 0) {
      var arr = [];
      var dataArr=[];
      console.log(oldData.receiveData.relateBean.length);
      var dataLength = oldData.receiveData.relateBean.length;
      // 循环出项目的个数,添加到arr中
      for (var a = 0; a < dataLength; a++) {
        arr.push(oldData.receiveData.relateBean[a].startDate)
        // 开抢时间
        var aaa = oldData.receiveData.relateBean[a].startDate;
        var str = aaa.slice(5, 10); 
        var d1 = str.replace(/\-/g, "月");
        dataArr.push(d1)
      }
      me.setData({
         actEndTimeList: arr,
         dataArr: dataArr
      })
      var interval = setInterval(function () {
        // 获取当前时间，同时得到活动结束时间数组
        let newTime = new Date().getTime();
        let endTimeList = me.data.actEndTimeList;
        let countDownArr = [];
        // 对结束时间进行处理渲染到页面
        endTimeList.forEach(o => {
          let endTime = new Date(Date.parse(o.replace(/-/g, "/"))).getTime();
          let obj = null;
          // 如果活动未结束，对时间进行处理
          if (endTime - newTime > 0) {
            let time = (endTime - newTime) / 1000;
            let day = parseInt(time / (60 * 60 * 24));
            let hou = parseInt(time % (60 * 60 * 24) / 3600);
            let min = parseInt(time % (60 * 60 * 24) % 3600 / 60);
            let sec = parseInt(time % (60 * 60 * 24) % 3600 % 60);
            obj = {
              day: me.timeFormat(day),
              hou: me.timeFormat(hou),
              min: me.timeFormat(min),
              sec: me.timeFormat(sec)
            }
          } else {
            obj = {
              day: '00',
              hou: '00',
              min: '00',
              sec: '00'
            }
          }
          countDownArr.push(obj);
        })
        // 渲染，然后每隔一秒执行一次倒计时函数
        me.setData({ countDownList: countDownArr })
      }.bind(me), 1000);
    }
  },

  methods: {
    timeFormat: function(param) {//小于10的格式化函数
      return param < 10 ? '0' + param : param;
    },
    clickLink: function (e) {
      let id=e.currentTarget.dataset.id;
      let promotionStatus=e.currentTarget.dataset.promotionstatus;
      let promotionType=e.currentTarget.dataset.promotiontype;
      if (promotionStatus == 1 && promotionType!=50){
          let a = "teMai.html?promotionId=" + id;
          app.globalData.linkEvent(a);
      } else if (promotionStatus == 0 || promotionType == 50) {
        let a = "promotion_detail.html?promotionId=" + id;
        app.globalData.linkEvent(a);
      }   
    },
  },
})